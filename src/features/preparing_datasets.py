import click
import pandas as pd
from typing import List
import category_encoders as ce
from sklearn.model_selection import train_test_split


COLUMNS = ["WindGustDir", "WindDir9am", "WindDir3pm"]
N_COMPONENTS = 16


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_file_paths", type=click.Path(), nargs=4)
def preparing_datasets(input_filepath, output_file_paths: List[str]):
    """
    Function prepares training and testing datasets from the input CSV file. It handles missing values, encodes categorical variables
    using a Hashing Encoder, and splits the data into training and testing sets. The processed datasets are saved to separate CSV files.

    :param input_filepath: Path to read the original DataFrame
    :param output_file_paths: List of two paths to save the processed training and testing DataFrames
    :return: None
    """

    df = pd.read_csv(input_filepath)
    train, test = train_test_split(
        df, test_size=0.2, random_state=2023, stratify=df["RainTomorrow"]
    )

    numerical = [col for col in train.columns if train[col].dtypes != "O"]

    for col in numerical:
        med_col = train[col].median()
        train[col].fillna(med_col, inplace=True)
        test[col].fillna(med_col, inplace=True)

    categorical_cols = list(train.select_dtypes(include=["object"]).columns)

    for col in categorical_cols:
        mode_col = train[col].mode()[0]
        train[col].fillna(mode_col, inplace=True)
        test[col].fillna(mode_col, inplace=True)

    x_train = train.drop("RainTomorrow", axis=1)
    y_train = train["RainTomorrow"]
    x_test = test.drop("RainTomorrow", axis=1)
    y_test = test["RainTomorrow"]

    hashing_enc = ce.HashingEncoder(cols=COLUMNS, n_components=N_COMPONENTS).fit(
        x_train, y_train
    )

    train = hashing_enc.transform(x_train.reset_index(drop=True))
    test = hashing_enc.transform(x_test.reset_index(drop=True))

    train.to_csv(output_file_paths[0], index=False)
    test.to_csv(output_file_paths[1], index=False)
    y_train.to_csv(output_file_paths[2], index=False)
    y_test.to_csv(output_file_paths[3], index=False)


if __name__ == "__main__":
    preparing_datasets()
