import click
import pandas as pd

NO_YES = {"No": 0, "Yes": 1}


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_file_path", type=click.Path())
def yo_mapping(input_filepath: str, output_file_path: str):
    """
    Function maps categorical values 'No' and 'Yes' in the 'RainToday' and 'RainTomorrow' columns of the input CSV file
    to numerical values 0 and 1, respectively. The transformed DataFrame is saved to a new CSV file.

    :param input_filepath: Path to read the original DataFrame
    :param output_file_path: Path to save the modified DataFrame with mapped 'RainToday' and 'RainTomorrow' columns
    :return: None
    """

    df = pd.read_csv(input_filepath)
    df["RainToday"] = df["RainToday"].map(NO_YES)
    df["RainTomorrow"] = df["RainTomorrow"].map(NO_YES)
    df = df.dropna(subset=["RainTomorrow"])

    df.to_csv(output_file_path, index=False)


if __name__ == "__main__":
    yo_mapping()
