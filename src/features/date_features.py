import click
import pandas as pd


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_file_path", type=click.Path())
def get_month(input_filepath: str, output_file_path: str):
    """
    Function extracts the month from the 'Date' column of the input CSV file, converts it to an integer, adds it as a new 'Month' column,
    drops the original 'Date' column, and saves the modified DataFrame to a new CSV file.

    :param input_filepath: Path to read the original DataFrame with the 'Date' column
    :param output_file_path: Path to save the modified DataFrame with the new 'Month' column
    :return: None
    """

    df = pd.read_csv(input_filepath)
    df["Year"] = df["Date"].apply(lambda x: x.split("-")[0]).astype("int")
    df["Month"] = df["Date"].apply(lambda x: x.split("-")[1]).astype("int")
    df.drop(["Date"], axis=1, inplace=True)
    df.to_csv(output_file_path, index=False)


if __name__ == "__main__":
    get_month()
