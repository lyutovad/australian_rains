import click
import pandas as pd


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_file_path", type=click.Path())
def change_locs(input_filepath: str, output_file_path: str):
    """
    Function transforms the 'Location' column in the input DataFrame by mapping each location to a new integer identifier based on the
    proportion of 'Yes' values. The transformed DataFrame is saved to a new CSV file.

    :param input_filepath: Path to read the original DataFrame
    :param output_file_path: Path to save the modified DataFrame with transformed 'Location' column
    :return: None
    """

    df = pd.read_csv(input_filepath)
    y_df = df["RainTomorrow"]
    data = pd.crosstab(index=df["Location"], columns=y_df, normalize="index")
    data = data.sort_values("Yes").reset_index()
    locs_dict = dict(zip(data.Location.values, data.index))
    df["Location"] = df["Location"].map(locs_dict)
    df.to_csv(output_file_path, index=False)


if __name__ == "__main__":
    change_locs()
