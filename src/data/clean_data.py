import click
import pandas as pd


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def drop_nans(input_path: str, output_path: str):
    """
    Removes rows with NaN values in the 'RainTomorrow' column from the input CSV file and saves the cleaned data to a new CSV file.
    :param input_filepath: Path to read the original DataFrame with all listings
    :param output_file_path: Path to save the filtered DataFrame
    :return: None
    """
    df = pd.read_csv(input_path)
    df = df.dropna(subset=["RainTomorrow"])
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    drop_nans()
