import os
from dotenv import load_dotenv
import click
import pandas as pd
import joblib as jb
import json
from xgboost import XGBClassifier

from typing import List
from sklearn.metrics import f1_score

import mlflow
from mlflow.models.signature import infer_signature

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)


@click.command()
@click.argument("input_paths", type=click.Path(exists=True), nargs=4)
@click.argument("output_paths", type=click.Path(), nargs=2)
def train(input_paths: List[str], output_paths: List[str]):
    """
    Trains an XGBoost model using training and testing datasets provided in CSV files,
    saves the trained model to the specified output path, and saves the classification
    report for the predictions on the test dataset.

    :param input_paths: List of four paths:
        - input_paths[0]: Path to the training features CSV file.
        - input_paths[1]: Path to the testing features CSV file.
        - input_paths[2]: Path to the training labels CSV file.
        - input_paths[3]: Path to the testing labels CSV file.
    :param output_paths: List of two paths:
        - output_paths[0]: Path to save the trained model.
        - output_paths[1]: Path to save the trained metrics.
    :return: None
    """
    with mlflow.start_run():
        x_train = pd.read_csv(input_paths[0])
        x_test = pd.read_csv(input_paths[1])
        y_train = pd.read_csv(input_paths[2])
        y_test = pd.read_csv(input_paths[3])
        assert isinstance(x_train, pd.DataFrame), "input[0] must be a valid dataframe"
        assert isinstance(x_test, pd.DataFrame), "input[1] must be a valid dataframe"
        assert isinstance(y_train, pd.DataFrame), "input[2] must be a valid dataframe"
        assert isinstance(y_test, pd.DataFrame), "input[3] must be a valid dataframe"

        params = {
            "learning_rate": 0.07633159573672302,
            "n_estimators": 589,
            "max_depth": 5,
            "min_child_weight": 67.5312046795583,
            "colsample_bytree": 0.7474271769434079,
        }

        xgb_model = XGBClassifier(**params, random_state=0, n_jobs=-1)
        xgb_model.fit(x_train, y_train)

        jb.dump(xgb_model, output_paths[0])

        y_pred = xgb_model.predict(x_test)
        score = dict(f1_score=f1_score(y_test, y_pred))

        with open(output_paths[1], "w") as score_file:
            json.dump(score, score_file, indent=4)

        signature = infer_signature(x_test, y_pred)

        mlflow.log_params(params)
        mlflow.log_metrics(score)
        mlflow.xgboost.log_model(
            xgb_model=xgb_model,
            artifact_path="model",
            registered_model_name="australian_rains_xgb",
            signature=signature,
        )

    experiment = dict(mlflow.get_experiment_by_name("Default"))
    experiment_id = experiment["experiment_id"]
    df = mlflow.search_runs([experiment_id])
    best_run_id = df.loc[0, "run_id"]
    print(best_run_id)


if __name__ == "__main__":
    train()
