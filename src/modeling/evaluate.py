import click
import pandas as pd
import joblib as jb
import json
from typing import List

from sklearn.metrics import f1_score


@click.command()
@click.argument("input_paths", type=click.Path(exists=True), nargs=3)
@click.argument("output_path", type=click.Path())
def evaluate(input_paths: List[str], output_path: str):
    """
    Function evaluates a trained model using a holdout dataset and saves the F1 score to a specified output path.
    :param input_paths: List of two paths, where the first path is to the holdout dataset CSV file and the second path is to the trained model file.
    :param output_path: Path to save the F1 score.
    :return: None
    """
    x_holdout = pd.read_csv(input_paths[0])
    y_holdout = pd.read_csv(input_paths[1])
    xgb_model = jb.load(input_paths[2])

    y_pred = xgb_model.predict(x_holdout)
    score = dict(f1_score=f1_score(y_holdout, y_pred))

    with open(output_path, "w") as score_file:
        json.dump(score, score_file, indent=4)


if __name__ == "__main__":
    evaluate()
